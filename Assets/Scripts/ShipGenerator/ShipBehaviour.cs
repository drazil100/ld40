using UnityEngine;

[ExecuteInEditMode]
public class ShipBehaviour : MonoBehaviour {
	public static int killCount = 0;

	public int pointValue = 0;
	public Ship ship = new Ship();
	public GameObject explosion = null;
	public GameObject damage = null;
	public bool isPlayer = false;
	public float despawnTimer = -1;
	public float currentHP = -1;
	public float maxHP = -1;
	public float damageHP = 1f;
	public AudioClip damageSound = null;
	public AudioClip destroySound = null;

	public void Start() {
		this.gameObject.GetComponent<MeshFilter>().mesh = ship.GenerateMesh();
		currentHP = maxHP;
	}

	public void Update() {
		if (gameObject.tag == "Player") {
			bool selfDestruct = Input.GetKey("p");
			if (selfDestruct) {
				currentHP -= 1f;
				GameManager.instance.SetHealth(currentHP / maxHP);
			}
		}

		if (despawnTimer >= 0) {
			despawnTimer -= Time.deltaTime;
			if (despawnTimer <= 0) {
				Object.Destroy(gameObject);
			}
		}
	}

	public void LateUpdate() {
		if (gameObject.tag == "Player" || TunnelGenerator.instance == null) return;

		if (Camera.main.transform.worldToLocalMatrix.MultiplyPoint3x4(gameObject.transform.position).z < -5) {
			// Went too far behind the camera, despawn
			Object.Destroy(gameObject);
		}

		if (gameObject.tag == "Bullet") return;

		TunnelGenerator.Node rail;
		float dist = TunnelGenerator.instance.NearestRail(this.gameObject.transform.position, out rail);
		if (dist > TunnelGenerator.instance.tunnelRadius * 0.9f) {
			GetComponent<Rigidbody>().velocity *= -1;
			transform.position += GetComponent<Rigidbody>().velocity * Time.deltaTime;
		}
	}

	public void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Powerup" || gameObject.tag == "Powerup") {
			// this is handled by PowerupBehaviour
			return;
		}
		if (other.gameObject.tag == "Bullet" || (other.gameObject.tag == "Enemy" && gameObject.tag == "Player")) {
			ShipBehaviour otherShip = other.gameObject.GetComponent<ShipBehaviour>();
			if (otherShip != null && isPlayer == otherShip.isPlayer) {
				// Can't hit yourself with your own bullets
				return;
			}
			currentHP -= otherShip.damageHP;
			if (gameObject.tag == "Player") {
				MoveShip ship = GameObject.FindWithTag("Rail").GetComponent<MoveShip>();
				if (GameManager.state != GameState.Playing || ship == null || ship.blinkTimer > 0f) {
					if (ship.blinkTimer > 0f) {
						currentHP += otherShip.damageHP;
					}
					return;
				}
				GameManager.instance.SetHealth(currentHP / maxHP);
				GameObject animation = null;
				if (currentHP <= 0) {
					animation = Object.Instantiate(explosion, ship.transform);
					GameManager.PlaySound(destroySound);
					GameManager.instance.LoseLife();
				} else {
					animation = Object.Instantiate(explosion, gameObject.transform.position - gameObject.transform.forward * 2, gameObject.transform.rotation);
					GameManager.PlaySound(damageSound);
					Light light = gameObject.GetComponent<Light>();
					light.color = Color.red;
					light.range = 3;
				}
				animation.GetComponent<Rigidbody>().velocity = Camera.main.transform.forward * TunnelGenerator.instance.speed * TunnelGenerator.instance.segmentSize * 1.1f;
			} else if (currentHP <= 0) {
				if (explosion != null) {
					Object.Instantiate(explosion, gameObject.transform.position, gameObject.transform.rotation);
					GameManager.PlaySound(destroySound);
				}
				if (gameObject.tag == "Enemy") {
					TunnelGenerator.Node rail;
					TunnelGenerator.instance.NearestRail(this.gameObject.transform.position, out rail);
					GameManager.score += pointValue;
					PointText pointText = Object.Instantiate(TunnelGenerator.instance.pointTextPrefab, gameObject.transform.position, Quaternion.LookRotation(rail.forward, rail.up)).GetComponent<PointText>();
					pointText.Text = string.Format("{0}", pointValue);
					killCount++;
					if (killCount % TunnelGenerator.instance.killsPerDrop == 0) {
						int dropIndex = (int)Random.Range(0, TunnelGenerator.instance.drops.Count);
						ShipBehaviour drop = TunnelGenerator.instance.drops[dropIndex];
						if (drop) {
							Object.Instantiate(drop, gameObject.transform.position, Quaternion.LookRotation(-rail.forward, rail.up));
						}
					}
				}
				Object.Destroy(gameObject);
			} else if (damage != null) {
				Object.Instantiate(damage, gameObject.transform.position, gameObject.transform.rotation);
				GameManager.PlaySound(damageSound);
			}
			Object.Destroy(other.gameObject);
		}
	}
}
