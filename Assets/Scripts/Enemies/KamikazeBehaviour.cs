using UnityEngine;

class KamikazeBehaviour : MonoBehaviour {
	public float leadingRange = .5f;
	public float homingPower = 25f;
	public float homingDelay = 0f;
	public float speed = 1f;

	public void FixedUpdate() {
		if (homingDelay > 0) {
			homingDelay -= Time.deltaTime;
			return;
		}
		if (!GameManager.IsRunning()) return;

		TunnelGenerator.Node rail;
		TunnelGenerator.instance.NearestRail(gameObject.transform.position, out rail);
		GameObject player = GameObject.FindGameObjectsWithTag("PlayerPositioner")[0];
		Rigidbody rb = gameObject.GetComponent<Rigidbody>();

		Vector3 forward = rail.forward;
		Vector3 targetPos = player.transform.position + player.transform.forward * leadingRange;
		float targetZ = Vector3.Project(targetPos, forward).z;
		float thisZ = Vector3.Project(gameObject.transform.position, forward).z;
		/*if (thisZ < targetZ) {
			// We're past our lead range, straight trajectories only now!
			return;
		}*/

		Vector3 targetVector = (targetPos - gameObject.transform.position).normalized;
		float maxDegrees = homingPower * Time.deltaTime;

		float angle = Vector3.Angle(forward, gameObject.transform.forward);
		if (angle > 360) {
			// If we're flying away from the player, arc around gracefully
			Vector3 axis = Vector3.Cross(forward, gameObject.transform.forward);
			rb.MoveRotation(Quaternion.AngleAxis(angle + maxDegrees * 100, axis));
		} else {
			// Otherwise interpolate the aim
			Quaternion currentRot = gameObject.transform.rotation;
			gameObject.transform.LookAt(targetPos);
			Quaternion targetRot = gameObject.transform.rotation;
			angle = Quaternion.Angle(currentRot, targetRot);
			if (angle > maxDegrees)
			{
				gameObject.transform.rotation = Quaternion.Slerp (currentRot, targetRot, maxDegrees / angle);
			}
		}
		rb.velocity = transform.forward * speed;
	}
}
