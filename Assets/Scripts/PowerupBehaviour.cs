using UnityEngine;

public enum PowerupType {
	Health,
}

public class PowerupBehaviour : MonoBehaviour {
	public PowerupType type = PowerupType.Health;
	public float speed = 100f;
	public GameObject explosion = null;
	public AudioClip collectSound = null;

	void FixedUpdate() {
		gameObject.transform.rotation *= Quaternion.AngleAxis(720 * Time.deltaTime, Vector3.up);
		Rigidbody rb = GetComponent<Rigidbody>();
		rb.velocity = (GameObject.FindWithTag("Player").transform.position - transform.position).normalized * speed;
	}

	public void OnTriggerEnter(Collider other) {
		if (other.tag != "Player") {
			return;
		}
		ShipBehaviour player = other.gameObject.GetComponent<ShipBehaviour>();
		GameManager.PlaySound(collectSound, 1);
		switch (type) {
			case PowerupType.Health:
				player.currentHP = Mathf.Clamp(player.currentHP + player.maxHP / 2f, 0, player.maxHP);
				GameManager.instance.SetHealth(player.currentHP / player.maxHP);
				break;
		}
		if (explosion != null) {
			Object.Instantiate(explosion, gameObject.transform.position, gameObject.transform.rotation);
		}
		Light light = player.GetComponent<Light>();
		light.color = Color.yellow;
		light.range = 3;
		Object.Destroy(gameObject);
	}
}
