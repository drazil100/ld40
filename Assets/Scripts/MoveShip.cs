﻿using UnityEngine;
using System.Collections;


public class MoveShip : LevelAwareBehaviour {
	public AudioClip fireSound = null;
	public bool inverted = true;
	public GameObject bulletPrefab;
	public Transform shipModel;
	public Transform shipPositioner;
	public Vector3 maxTurnRotationNormal = new Vector3 (35f, 35f, -45f);
	public Vector3 maxMoveSpeed = new Vector3 (0f, 10f, 10f);
	public Vector3 maxTurnSpeed = new Vector3 (0f, 20f, 0f);
	public float dampening = 0.05f;
	public float bulletSpeed = 10f;

	[HideInInspector]
	public float blinkTimer = 0f;
	private float fireCooldown = 0f;
	private float horizontal = 0f;
	private float vertical = 0f;
	private float horizontal2 = 0f;
	private float vertical2 = 0f;
	private bool hasAimAssist = false;
	private Vector3 aimAssist;
	private int shots = 0;

	public override void OnGameStart() {
		GameManager.lives = 3;
		GameManager.score = 0;
		GameManager.health = 0.001f;
		ShipBehaviour shipBehaviour = shipModel.GetComponent<ShipBehaviour>();
		shipBehaviour.currentHP = shipBehaviour.maxHP;
	}

	public override void OnLevelStart() {
		if (GameManager.state != GameState.Title && GameManager.state != GameState.GameOver) {
			blinkTimer = 2.5f;
		}
	}

	public override void OnPlayerDamage() {
		blinkTimer = 0.5f;
	}

	public override void OnPlayerDeath() {
		blinkTimer = 4f;
		ShipBehaviour shipBehaviour = shipModel.GetComponent<ShipBehaviour>();
		shipBehaviour.currentHP = shipBehaviour.maxHP;
	}

	void Update()
	{
		if (GameManager.state == GameState.Title || GameManager.state == GameState.GameOver) {
			shipModel.GetComponent<MeshRenderer>().enabled = false;
			return;
		}
		if (blinkTimer > 0) {
			blinkTimer -= Time.deltaTime;
			if (blinkTimer <= 0) {
				shipPositioner.localScale = Vector3.one;
				shipModel.GetComponent<MeshRenderer>().enabled = true;
				if (GameManager.state == GameState.Spawning) {
					GameManager.instance.StartLife();
				}
			} else if (blinkTimer > 2) {
				shipModel.GetComponent<MeshRenderer>().enabled = false;
				return;
			} else {
				float spawnProgress = (2.0f - blinkTimer) / 2.0f;
				if (GameManager.state == GameState.Dead || GameManager.state == GameState.LevelLoad) {
					GameManager.instance.SpawnLife();
				} else if (GameManager.state == GameState.Spawning) {
					ShipBehaviour shipBehaviour = shipModel.GetComponent<ShipBehaviour>();
					GameManager.health = Mathf.Lerp(0, shipBehaviour.currentHP / shipBehaviour.maxHP, spawnProgress);
				}
				shipPositioner.localScale = Vector3.one * spawnProgress;
				shipModel.GetComponent<MeshRenderer>().enabled = ((blinkTimer * 20) % 2 < 1);
			}
		}

		Light light = shipModel.GetComponent<Light>();

		if (GameManager.state != GameState.Playing && GameManager.state != GameState.Spawning) {
			light.range = 0;
			return;
		}

		if (light.range > 0) {
			light.range -= Time.deltaTime * 5f;
		}

		float hInput = Input.GetAxis ("Horizontal");
		float vInput = Input.GetAxis ("Vertical");
		bool fireButton = Input.GetButton("Fire1");
		if (Input.GetButtonDown ("Fire1"))
		{
			shots = 3;
		}
		bool fire = fireButton && fireCooldown <= 0;

		bool heal = GameManager.state == GameState.Playing && !fireButton && fireCooldown <= 0 && blinkTimer <= 0;

		//Debug.Log("(" + horizontal + ", " + vertical + ")");

		if (inverted)
			vInput = -vInput;

		float hInput2 = hInput;
		float vInput2 = vInput;

		float sign = Mathf.Sign (hInput);
		if (Mathf.Sign (horizontal) == sign /*&& Mathf.Abs(hInput) > Mathf.Abs(horizontal)*/)
		{
			if (Mathf.Abs (hInput) > Mathf.Abs (horizontal))
			{
				hInput2 = (Mathf.Abs (hInput) - Mathf.Abs (horizontal)) * sign;
			}
			else
			{
				hInput2 = 0f;
			}
		}

		sign = Mathf.Sign (vInput);
		if (Mathf.Sign (vertical) == sign /*&& Mathf.Abs(vInput) > Mathf.Abs(vertical)*/)
		{
			if (Mathf.Abs (vInput) > Mathf.Abs (vertical))
			{
				vInput2 = (Mathf.Abs (vInput) - Mathf.Abs (vertical)) * sign;
			}
			else
			{
				vInput2 = 0f;
			}
		}

		horizontal = Mathf.Clamp (horizontal + (hInput2 * dampening * Time.deltaTime * 60), -1f, 1f);
		vertical = Mathf.Clamp (vertical + (vInput2 * dampening * Time.deltaTime * 60), -1f, 1f);
		horizontal2 = Mathf.Clamp (horizontal2 + (hInput2 * dampening * Time.deltaTime * 60), -1f, 1f);
		vertical2 = Mathf.Clamp (vertical2 + (vInput2 * dampening * Time.deltaTime * 60), -1f, 1f);

		//Debug.Log( "("+ hInput2 + ", " + vInput2 +") (" + horizontal + ", " + vertical + ")");

		if (hInput > -0.01f && hInput < 0.01f)
		{
			horizontal = GeneralFunctions.Approach (horizontal, 0f, dampening, 0.01f);
		}
		if (vInput > -0.01f && vInput < 0.01f)
		{
			vertical = GeneralFunctions.Approach (vertical, 0f, dampening, 0.01f);
		}


		shipModel.transform.Rotate (-vertical * maxTurnSpeed.x * Time.deltaTime, horizontal * maxTurnSpeed.y * Time.deltaTime, 0);
		shipPositioner.transform.Translate (horizontal * maxMoveSpeed.x * Time.deltaTime, vertical * maxMoveSpeed.y * Time.deltaTime, maxMoveSpeed.z * Time.deltaTime);

		Camera.main.transform.localEulerAngles = new Vector3 (0, 0, 0);
		Vector3 screenPos = Camera.main.WorldToScreenPoint(shipPositioner.transform.position);
		screenPos.x = Mathf.Clamp(screenPos.x, 0, Camera.main.pixelWidth);
		float offset = (screenPos.x / (float)Camera.main.pixelWidth);
		screenPos.y = Mathf.Clamp(screenPos.y, 0, Camera.main.pixelHeight);
		shipPositioner.transform.position = Camera.main.ScreenToWorldPoint(screenPos);


		float interpolation = horizontal2 / 2 + 0.5f;
		float interpolation2 = 1 - (vertical2 / 2 + 0.5f);


		Vector3 turn = maxTurnRotationNormal;

		float x = Mathf.LerpAngle (-turn.x, turn.x, interpolation2);
		float y = Mathf.LerpAngle (-turn.y, turn.y, interpolation);
		float z = Mathf.LerpAngle (-turn.z, turn.z, interpolation);


		float cy = Mathf.LerpAngle (-10, 10, offset);

		shipModel.transform.localEulerAngles = new Vector3 (x, y, z);
		//Camera.main.transform.localEulerAngles = new Vector3 (0, cy, 0);

		RaycastHit hit = new RaycastHit();
		int range = 0;
		const float step = 10f;
		Vector3 target = Vector3.zero;
		/*while (range < 5) {
			hasAimAssist = Physics.BoxCast(shipPositioner.position + shipModel.transform.forward * range * step, Vector3.one * (range + 1) * 0.5f, shipModel.transform.forward, out hit, Quaternion.identity, Mathf.Infinity, 1 << 8);
			if (hasAimAssist) {
				aimAssist = hit.collider.gameObject.transform.position;
				target = Vector3.MoveTowards(aimAssist, shipPositioner.position, -2.0f);
				float dist = (aimAssist - shipPositioner.position).magnitude;
				aimAssist += hit.rigidbody.velocity * (dist / bulletSpeed);
				break;
			}
			range++;
		}*/

		fireCooldown -= Time.deltaTime;
		if (fire && shots > 0) {
			fireCooldown = 0.075f;
			shots--;
			GameObject bullet = Object.Instantiate(bulletPrefab, shipPositioner.position + shipModel.transform.forward * (2 + bulletPrefab.transform.localScale.z), shipModel.rotation);
			GameManager.PlaySound(fireSound);
			bullet.GetComponent<ShipBehaviour>().isPlayer = true;
			if (hasAimAssist) {
				bullet.GetComponent<Rigidbody>().velocity = Vector3.Normalize(aimAssist - shipModel.transform.position) * bulletSpeed;
			} else {
				bullet.GetComponent<Rigidbody>().velocity = shipModel.transform.forward * bulletSpeed;
			}
		} else if (heal) {
			fireCooldown = 0.05f;
			ShipBehaviour shipBehaviour = shipModel.GetComponent<ShipBehaviour>();
			if (shipBehaviour.currentHP < shipBehaviour.maxHP) {
				// Very very slow heal when alive, not shooting, and not flashing
				shipBehaviour.currentHP += Time.deltaTime * 0.1f;
				GameManager.instance.SetHealth(shipBehaviour.currentHP / shipBehaviour.maxHP);
			}
		}
	}

	private float transition (float current, float target, float percentPerFrame)
	{
		return current + (target - current) * Mathf.Clamp (percentPerFrame, 0, 1);
	}

	public override void OnPlayerSpawn() {
		GameManager.health = 0.01f;
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
		shipPositioner.position = Vector3.zero;
		shipModel.rotation = Quaternion.identity;
		horizontal = vertical = horizontal2 = vertical2 = 0;
	}
}
