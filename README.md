Ludum Dare 40
-------------
**Theme:** The more you have, the worse it is

**Team:** [Adam Higerd](https://ldjam.com/users/coda-highland) and [Austin Allman](https://ldjam.com/users/drazil100)

Network Invader
===============
They say the Internet is a series of tubes. Fly your way through the network and eliminate the invaders!

Controls
--------
* Move: Arrow keys
* Fire: Space
* Pause: Escape

Credits
-------
Programming, graphics, and sound: [Adam Higerd](https://ldjam.com/users/coda-highland) and [Austin Allman](https://ldjam.com/users/drazil100)

Background music and level transition sound effect: Various pieces from [Soundimage.org](http://soundimage.org) by Eric Matyas

Logo background texture: [Soundimage.org](http://soundimage.org) by Eric Matyas

License
=======
Copyright (c) 2017 Adam Higerd and Austin Allman

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

